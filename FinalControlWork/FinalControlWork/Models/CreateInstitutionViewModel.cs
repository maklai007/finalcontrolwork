﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FinalControlWork.Models
{
    public class CreateInstitutionViewModel
    {
        [Required(ErrorMessage = "Укажите название")]
        [Display(Name = "Название заведения")]
        public string NameInstitution { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        [Required(ErrorMessage = "Укажите описание")]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        public IFormFile Logo { get; set; }
    }
}
