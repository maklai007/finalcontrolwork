﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalControlWork.Models
{
    public class Institution
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string NameInstitution { get; set; }
        public string Description { get; set; }
        public decimal Rating { get; set; }
    }
}
