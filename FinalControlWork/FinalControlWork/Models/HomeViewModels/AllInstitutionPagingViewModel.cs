﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalControlWork.Models.ViewModels;

namespace FinalControlWork.Models.HomeViewModels
{
    public class AllInstitutionPagingViewModel
    {
        public List<ShowAllInstitutionViewModel> ShowAllInstitutionViewModels { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
