﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalControlWork.Models
{
    public class ShowAllInstitutionViewModel
    {
        public Institution Institutions { get; set; }
        public List<LogoImage> logoImages { get; set; }
        public List<Review> Reviews { get; set; }
    }
}
