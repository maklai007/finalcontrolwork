﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalControlWork.Models
{
    public class LogoImage
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }
        public string Logo { get; set; }
    }
}
