﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalControlWork.Models.ViewModels
{
    public class FilterViewModel
    {
        public string SelectedName { get; private set; }
        
        public FilterViewModel(
            string name)
        {
            SelectedName = name;
        }
    }
}
