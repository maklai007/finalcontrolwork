﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalControlWork.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string ReviewUser { get; set; }
        public int InstitutionRating { get; set; }
        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }
        public DateTime DateOfCreation { get; set; }
    }
}
