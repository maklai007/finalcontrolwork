﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FinalControlWork.Data;
using Microsoft.AspNetCore.Mvc;
using FinalControlWork.Models;
using FinalControlWork.Models.HomeViewModels;
using FinalControlWork.Models.ViewModels;
using FinalControlWork.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace FinalControlWork.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly PagingService _pagingService;
        public HomeController(
            ApplicationDbContext context, PagingService pagingService)
        {
            _context = context;
            _pagingService = pagingService;
        }

        [HttpGet]
        public IActionResult Index(string name, int page=1)
        {
            IEnumerable<Institution> allInstitutions = _context.Institutions;
            List<ShowAllInstitutionViewModel> models = new List<ShowAllInstitutionViewModel>();

            foreach (var institution in allInstitutions)
            {
                models.Add(new ShowAllInstitutionViewModel
                {
                    Institutions = institution,
                    logoImages = _context.LogoImages.Where(l => l.InstitutionId == institution.Id).ToList(),
                    Reviews = _context.Reviews.Where(r => r.InstitutionId == institution.Id).ToList()
                });
            }

            PagedObject<ShowAllInstitutionViewModel> objectPaging = _pagingService.DoPage(models.AsQueryable(), page);
            AllInstitutionPagingViewModel viewModel = new AllInstitutionPagingViewModel
            {
                FilterViewModel = new FilterViewModel(name),
                PageViewModel = new PageViewModel(objectPaging.Count, page, objectPaging.PageSize),
                ShowAllInstitutionViewModels = objectPaging.Objects
            };

            return View(viewModel);
        }

        [HttpGet]
        public IActionResult ViewInstitution(int id)
        {
            Institution institution = _context.Institutions.FirstOrDefault(i => i.Id == id);
            ShowAllInstitutionViewModel model = new ShowAllInstitutionViewModel
            {
                Institutions = institution,
                logoImages = _context.LogoImages.Where(l => l.InstitutionId == id).ToList(),
                Reviews = _context.Reviews.Where(r => r.InstitutionId == id).ToList()
            };
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
